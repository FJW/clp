Command-Line-Parser (clp)
=========================

This is Version 2. Version 1 is available in the [V1 branch](https://gitlab.com/FJW/clp/-/tree/V1). The two versions are not compatible since V2 turned the parameter-name from a runtime-argument into a template-paramter (`clp::bool_flag{'b',"bool"}` became `clp::bool_flag<"b,bool">{}`). While this requires manual changes on the user-side, I believe that they are worth it: Not only does this allow for more compile-time checking and simplifies the code a little, but it allows to get at the parsed arguments via `parsed.get<"long-name">()`, which was impossible before and for all I know might still be a unique feature for commandline-parsers in C++.

An example shows the intended way to use this library best:

```cpp
#include <iostream>
#include <string>

#include <clp/clp.hpp>

int main(int argc, char** argv) {
	// Use abbreviated names such as `req` instead of `clp::required_argument`:
	using namespace clp::abbr;
	const auto parser = arg_parser{
	        req<"i,int", int>{desc{"An integer that has to be provided"}},
	        opt<"float", double>{desc{"An optional float without short name."}},
	        def<"s,str", std::string>{"some default value", desc{"a string with a default"}},
	        flg<"h,help">{desc{"a flag that indicates whether the argument was passed"}},
	        ctr<"v,verbose">{desc{"a counter of how often the argument was passed"}},
	        opt<"foo", double>{deps{{"float", "verbose"}},
	                           desc{"passing foo requires float and verbose"}}};
	try {
		const auto parsed = parser.parse(argc, argv);
		// for few arguments tuple-unpacking is easiest:
		const auto [i, f, s, h, v, foo] = parsed;
		std::cout << i << ", " << f.value_or(23) << ", " << s << '\n';
		if (v > 3) {
			std::cout << "Very verbose output\n";
		}
		// Alternatively the values can be accessed like this:
		if (parsed.get<"help">()) {
			parser.print_help(argv[0], std::cout);
		}
	} catch (clp::bad_argument_list& e) {
		std::cerr << "Error: " << e.what() << '\n';
		parser.print_help(argv[0], std::cerr);
		return 1;
	}
}
```

Assuming you installed clp system-wide, you can add it to CMake like this:

```
# ...

find_package(clp REQUIRED)

# ...

add_executable(your_executable ${source_files})
target_link_libraries(your_executable clp::clp)

```

Currently implemented features
------------------------------

* Very user-friendly interface.
* Long and short forms of argument-names.
* Required, optional and defaulted arguments.
* Boolean flags and counters.
* Collected arguments (multiple invocations create a list of return-values).
* Support for any type that can be read from a `std::istream`.
* Highly extendable argument-types.
* Dependencies among arguments.
* No dependencies on any other libraries; this is pure ISO-C++.
	* The unittests depend on Catch2, but building them is optional.
* Header-only.
* Seemingly working CMake-integration.
* Automatic generation of help.
* Compiles cleanly with `-Wall -Wextra -Wpedantic -Wconversion -Wsign-conversion` on GCC.
* Basic support for positional arguments.

Possible future-features
------------------------

Once modules are practical (which largely hinges on CMake-support at this point) this project will move to them.

Other that, I’m in principle interested in the following features, but there is no telling when, if ever, I'll implement them:

* Better compiler-error-messages.
* Faster Compilation.
* Combined short arguments (`"-abc"` being equivalent to `"-a", "-b", "-c"`).
* Better support for positional arguments.
* Anything else that would turn this into the most epic argument-parser ever. 😉︎

License
-------

LGPL v3. As this is a header-only library, my understanding is that you can do almost anything you want,
as long as you don't change the library itself. In that case just publish the changed version of the library.
