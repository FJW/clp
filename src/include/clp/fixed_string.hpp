#ifndef CLP_FIXED_STRING_HPP
#define CLP_FIXED_STRING_HPP

#include <algorithm>
#include <array>
#include <cstdint>
#include <string>
#include <string_view>
#include <tuple>
#include <type_traits>

namespace clp::impl {

template <std::size_t N>
struct fixed_string {
	constexpr fixed_string(const char* s) { std::copy(s, s + N, this->data.begin()); }
	constexpr auto size() const { return data.size(); }
	constexpr auto empty() const { return data.empty(); }
	std::string to_string() const { return {data.begin(), data.end()}; }
	constexpr std::string_view to_string_view() const { return {data.begin(), data.end()}; }
	std::array<char, N> data;
	auto operator<=>(const fixed_string&) const = default;
};

template <std::size_t N>
fixed_string(char const (&)[N]) -> fixed_string<N - 1>;

template <fixed_string S>
constexpr auto fixed_string_literal = S;

template <fixed_string S>
struct index {
	std::size_t index;
};

template <fixed_string... S>
struct string_list {
	static constexpr auto size() { return sizeof...(S); }
	template <std::size_t I>
	static constexpr auto get() {
		return std::get<I>(std::tuple(S...));
	}
	static constexpr auto back() { return get<size() - 1>(); }
	static constexpr auto front() { return get<0>(); }
	auto operator<=>(const string_list&) const = default;
};

template <fixed_string... L, fixed_string... R>
constexpr auto operator+(string_list<L...>, string_list<R...>) {
	return string_list<L..., R...>{};
}

template <fixed_string... S>
constexpr bool contains_duplicates(string_list<S...>) {
	auto arr = std::array{S.to_string_view()...};
	std::sort(arr.begin(), arr.end());
	return std::unique(arr.begin(), arr.end()) != arr.end();
}

template <fixed_string... S>
constexpr bool contains_non_empty_duplicates(string_list<S...>) {
	auto arr = std::array{S.to_string_view()...};
	std::sort(arr.begin(), arr.end());
	const auto start =
	        std::find_if_not(arr.begin(), arr.end(), [](auto sv) { return sv.empty(); });
	return std::unique(start, arr.end()) != arr.end();
}

template <fixed_string S, char Sep>
constexpr auto split() {
	constexpr auto pos = std::find(S.data.begin(), S.data.end(), Sep);
	constexpr auto head_size = std::distance(S.data.begin(), pos);
	constexpr auto head = fixed_string<head_size>(S.data.data());
	if constexpr (head_size == S.size()) {
		return string_list<head>{};
	} else {
		constexpr auto tail =
		        fixed_string<S.size() - head_size - 1>(S.data.data() + head_size + 1);
		return string_list<head>{} + split<tail, Sep>();
	}
}

template <fixed_string... IArgs, std::size_t... I>
constexpr auto make_index_tuple(string_list<IArgs...>, std::index_sequence<I...>) {
	return std::tuple{index<IArgs>{I}...};
}

template <fixed_string S, fixed_string... IArgs, typename... VArgs>
constexpr auto get_value(string_list<IArgs...> names, const std::tuple<VArgs...>& value_tuple) {
	constexpr auto index_tuple =
	        make_index_tuple(names, std::make_index_sequence<sizeof...(IArgs)>{});
	return std::get<std::get<index<S>>(index_tuple).index>(value_tuple);
}

template <fixed_string S>
constexpr auto extract_short_name() {
	constexpr auto list = split<S, ','>();
	static_assert(0 < list.size() and list.size() <= 2);
	if constexpr (list.size() == 1) {
		return fixed_string_literal<"">;
	} else {
		return list.front();
	}
}
template <fixed_string S>
constexpr auto extract_long_name() {
	constexpr auto list = split<S, ','>();
	static_assert(0 < list.size() and list.size() <= 2);
	constexpr auto list_2 = split<list.back(), ':'>();
	static_assert(0 < list_2.size() and list_2.size() <= 2);
	return list_2.front();
}

template <fixed_string S>
constexpr auto extract_identifier_name() {
	constexpr auto list = split<S, ','>();
	static_assert(0 < list.size() and list.size() <= 2);
	constexpr auto list_2 = split<list.back(), ':'>();
	static_assert(0 < list_2.size() and list_2.size() <= 2);
	return list_2.back();
}

} // namespace clp::impl

#endif // CLP_FIXED_STRING_HPP
