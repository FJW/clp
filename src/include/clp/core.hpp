#ifndef CLP_CORE_HPP
#define CLP_CORE_HPP

#include <algorithm>
#include <functional>
#include <memory>
#include <numeric>
#include <ostream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "fixed_string.hpp"
#include "type_support.hpp"

namespace clp {

constexpr auto unlimited_invocations = std::numeric_limits<unsigned>::max();

struct invocation_range {
	unsigned min = 0u;
	unsigned max = 1u;
};

struct argument_count {
	unsigned n = 1u;
};

struct dependencies {
	std::vector<std::string> deps;
};

struct description {
	std::string desc;
};

template <typename Storage_Type>
struct parsing_state {
	Storage_Type value;
	unsigned invocations = 0;
};

struct option_info {
	char short_name = '\0';
	std::string long_name = "";
	std::string argument_type = "";
	std::string default_argument = "";
	bool required = false;
	std::string description = "";
};

template <impl::fixed_string Name, typename Self, typename Result, typename Storage_Type>
class option_base {
public:
	template <typename... Settings>
	option_base(Storage_Type default_value, Settings... settings)
	        : m_initial_state{std::move(default_value), 0} {
		(void)std::initializer_list<int>{(setup(settings), 0)...};
	}

	static constexpr auto name() { return impl::extract_identifier_name<Name>(); }

	using result_type = Result;
	using storage_type = Storage_Type;
	using state = parsing_state<Storage_Type>;

	state initial_state() const { return m_initial_state; }

	result_type get(const state& state) const {
		verify_sufficient_invocations(state);
		if constexpr (!std::is_same_v<result_type, storage_type>) {
			return get_self().finalise_result(state);
		} else {
			return state.value;
		}
	}

	result_type get(state&& state) const {
		verify_sufficient_invocations(state);
		if constexpr (!std::is_same_v<result_type, storage_type>) {
			return get_self().finalise_result(std::move(state));
		} else {
			return std::move(state.value);
		}
	}

	void parse(std::vector<std::string> args, state& state) const {
		++state.invocations;
		if (state.invocations > m_allowed_invocations.max) {
			throw bad_argument{
			        "Too many arguments of type " + long_name_as_string() +
			        ". Maximum allowed: " + std::to_string(m_allowed_invocations.max)};
		}
		get_self().parse_argument(std::move(args), state);
	}

	static constexpr auto long_name() { return impl::extract_long_name<Name>(); }
	static constexpr auto short_name() { return impl::extract_short_name<Name>(); }
	static_assert(short_name().size() <= 1);
	static constexpr char short_name_as_char() {
		return short_name().size() == 0 ? '\0' : short_name().data[0];
	}
	static std::string long_name_as_string() { return long_name().to_string(); }

	unsigned arg_count() const { return m_allowed_arguments; }
	const std::vector<std::string>& deps() const { return m_dependencies; }

	option_info info() const {
		using impl::value_to_string;
		auto argument_type = get_self().argument_type_as_string();
		auto default_argument = value_to_string(m_initial_state.value, 0);
		auto required = m_allowed_invocations.min > 0;
		return {short_name_as_char(),
		        long_name_as_string(),
		        argument_type,
		        default_argument,
		        required,
		        m_desc};
	}

	static constexpr bool is_positional_argument() { return false; }

	// These are intended to be proteced, but for some reason CRTP doesn't count as regular
	// inheritance:
	option_base<Name, Self, Result, Storage_Type>& get_base() & { return *this; }

	const option_base<Name, Self, Result, Storage_Type>& get_base() const& { return *this; }

	option_base<Name, Self, Result, Storage_Type>&& get_base() && { return *this; }

protected:
	std::string argument_type_as_string() const {
		using impl::type_to_string;
		return type_to_string(parse_type<result_type>{}, 0);
	}

private:
	void setup(invocation_range invocations) { m_allowed_invocations = invocations; }
	void setup(argument_count arg_count) { m_allowed_arguments = arg_count.n; }
	void setup(description desc) { m_desc = desc.desc; }
	void setup(dependencies deps) { m_dependencies = std::move(deps.deps); }
	Self& get_self() & { return static_cast<Self&>(*this); }
	const Self& get_self() const& { return static_cast<const Self&>(*this); }
	Self&& get_self() && { return static_cast<Self&&>(*this); }

	void verify_sufficient_invocations(const state& state) const {
		if (state.invocations < m_allowed_invocations.min) {
			throw bad_argument{"Insufficient instances of " + long_name_as_string() +
			                   " provided. Minimum necessary: " +
			                   std::to_string(m_allowed_invocations.min)};
		}
	}

	std::string m_desc;
	state m_initial_state;
	invocation_range m_allowed_invocations = {};
	unsigned m_allowed_arguments = 1u;
	std::vector<std::string> m_dependencies;
};

namespace impl {
inline std::pair<std::vector<std::string>, std::ptrdiff_t>
take_n_args(std::vector<std::string>::const_iterator it, const std::vector<std::string>& args,
            std::size_t n, std::string_view possible_arg, std::string_view parameter_name) {
	const auto remaining_elements = static_cast<std::size_t>(std::distance(it, args.end()));
	auto ret = std::vector<std::string>{};
	if (not possible_arg.empty()) {
		ret.emplace_back(possible_arg);
		--n;
	}
	if (n > remaining_elements) {
		throw bad_argument{"not enough arguments for paramter " +
		                   std::string{parameter_name}};
	}
	std::copy_n(it, n, std::back_inserter(ret));
	return {ret, static_cast<std::ptrdiff_t>(n)};
}
} // namespace impl

template <impl::string_list Typenames, typename... Values>
struct result {
	std::tuple<Values...> values;
	template <impl::fixed_string S>
	auto get() const {
		return impl::get_value<S>(Typenames, values);
	}
	template <std::size_t I>
	auto get() const {
		return std::get<I>(values);
	}
};
} // namespace clp

namespace std {
template <::clp::impl::string_list S, class... Types>
struct tuple_size<::clp::result<S, Types...>> : integral_constant<size_t, sizeof...(Types)> {};
template <size_t I, clp::impl::string_list S, class... Types>
struct tuple_element<I, ::clp::result<S, Types...>> : tuple_element<I, tuple<Types...>> {};
} // namespace std

namespace clp {
template <typename... Parameters>
class arg_parser {
public:
	arg_parser(Parameters... params) : m_params{std::move(params)...} {
		setup_parameters(indeces{});
	}

	using long_names = impl::string_list<Parameters::long_name()...>;
	static_assert(not impl::contains_duplicates(long_names{}));
	using names = impl::string_list<Parameters::name()...>;
	static_assert(not impl::contains_non_empty_duplicates(names{}));
	using short_names = impl::string_list<Parameters::short_name()...>;
	static_assert(not impl::contains_non_empty_duplicates(short_names{}));

	using result_type = result<names{}, typename Parameters::result_type...>;
	using storage_type = std::tuple<parsing_state<typename Parameters::storage_type>...>;

	storage_type gen_parsing_state() const { return gen_parsing_state_impl(indeces{}); }

	result_type parse(const std::vector<std::string>& args) const {
		auto storage = gen_parsing_state();
		auto provided_args = std::unordered_set<std::string>{};
		auto needed_args = std::unordered_set<std::string>{};
		auto next_positional_index = std::size_t{};
		for (auto it = args.begin(); it != args.end();) {
			if (it->empty() or *it == "-") {
				throw bad_argument{"invalid argument"};
			} else if (it->front() != '-') {
				if (m_positional_args.size() <= next_positional_index) {
					throw bad_argument{"too many positional arguments"};
				}
				// positional argument
				auto [opt_args, n] = impl::take_n_args(
				        it, args,
				        m_positional_args[next_positional_index].argument_count, "",
				        "");
				m_positional_args[next_positional_index++].parser(
				        std::move(opt_args), storage);
				it += n;
			} else {
				auto [name, data_it, first_arg] = next_option(*it);
				++it;
				auto [opt_args, n] = impl::take_n_args(
				        it, args, data_it->second.argument_count, first_arg, name);
				it += n;
				data_it->second.parser(std::move(opt_args), storage);
				provided_args.insert(name);
				needed_args.insert(data_it->second.dependencies.begin(),
				                   data_it->second.dependencies.end());
			}
		}
		for (auto&& arg : needed_args) {
			if (provided_args.count(arg) == 0) {
				throw bad_argument{arg + " not provided but is required by at "
				                         "least one other argument."};
			}
		}
		return return_results(indeces{}, std::move(storage));
	}

	result_type parse(int argc, char** argv) const { return parse({argv + 1, argv + argc}); }

	void print_help(std::string_view program_name, std::ostream& s) const {
		const auto information = get_information_vector(indeces{});
		struct lengths_t {
			std::size_t long_name_length = 0;
			std::size_t typename_length = 0;
			std::size_t default_value_length = 0;
		};
		const auto [long_name_length, typename_length, default_value_length] =
		        std::accumulate(
		                information.begin(), information.end(), lengths_t{},
		                [](lengths_t l, const option_info& i) {
			                return lengths_t{
			                        std::max(l.long_name_length, i.long_name.size()),
			                        std::max(l.typename_length, i.argument_type.size()),
			                        std::max(l.default_value_length,
			                                 i.default_argument.size())};
		                });
		const auto fill = [](std::size_t i) { return std::string(i, ' '); };

		s << "Usage: " << program_name << " [options]\n";
		for (const auto& info : information) {
			s << '\t';
			if (auto c = info.short_name; c != 0) {
				s << '-' << c << ", ";
			} else {
				s << "    ";
			}
			assert(not info.argument_type.empty());
			const auto arg_type = info.required ? "⟨" + info.argument_type + "⟩"
			                                    : '[' + info.argument_type + ']';
			s << "--" << info.long_name
			  << fill(long_name_length - info.long_name.size())
			  << (info.argument_type.empty() ? "   " : " : ") << arg_type
			  << fill(typename_length - info.argument_type.size())
			  << (info.default_argument.empty() ? "   " : " = ")
			  << info.default_argument
			  << fill(default_value_length - info.default_argument.size()) << " "
			  << info.description << '\n';
		}
	}

private:
	using indeces = std::make_index_sequence<sizeof...(Parameters)>;

	struct option_data {
		std::function<void(std::vector<std::string>, storage_type& out)> parser;
		unsigned argument_count;
		std::vector<std::string> dependencies;
	};

	template <std::size_t... I>
	storage_type gen_parsing_state_impl(std::index_sequence<I...>) const {
		return storage_type{std::get<I>(m_params).initial_state()...};
	}

	std::tuple<std::string,
	           typename std::unordered_map<std::string, option_data>::const_iterator,
	           std::string_view>
	next_option(const std::string& arg) const {
		auto option_name = std::string{}; // arg.substr(2);
		auto first_arg = std::string_view{};
		if (char c = arg[1]; c != '-') { // short option
			const auto it = m_short_long_map.find(c);
			if (it == m_short_long_map.end()) {
				throw bad_argument{std::string{"unknown short option: "} + c};
			}
			option_name = it->second;
			if (arg.size() > 2u) {
				first_arg = arg;
				first_arg.remove_prefix(2);
			}
		} else { // long option
			const auto i = arg.find('=', 2);
			if (i == std::string::npos) {
				option_name = arg.substr(2);
			} else {
				option_name = arg.substr(2, i - 2);
				first_arg = arg;
				first_arg.remove_prefix(i + 1);
			}
		}
		const auto it = m_adders.find(option_name);
		if (it == m_adders.end()) {
			throw bad_argument{"unknown option: " + arg};
		}
		return {option_name, it, first_arg};
	}

	template <std::size_t... I>
	auto setup_parameters(std::index_sequence<I...>) {
		(void)std::initializer_list<int>{[&](auto& param) {
			// for some reason clang rejects param.is_positional_argument() which really
			// should work!
			constexpr auto positional =
			        std::decay_t<decltype(param)>::is_positional_argument();
			if constexpr (not positional) {
				m_adders[param.long_name_as_string()] = option_data{
				        [ptr = &param](std::vector<std::string> args,
				                       storage_type& s) {
					        ptr->parse(std::move(args), std::get<I>(s));
				        },
				        param.arg_count(), param.deps()};
				if (param.short_name_as_char() != 0) {
					m_short_long_map[param.short_name_as_char()] =
					        param.long_name_as_string();
				}
			} else {
				m_positional_args.push_back(option_data{
				        [ptr = &param](std::vector<std::string> args,
				                       storage_type& s) {
					        ptr->parse(std::move(args), std::get<I>(s));
				        },
				        param.arg_count(), param.deps()});
			}
			return 0;
		}(std::get<I>(m_params))...};
	}

	template <std::size_t... I>
	result_type return_results(std::index_sequence<I...>, storage_type storage) const {
		return result_type{{(std::get<I>(m_params).get(std::get<I>(storage)))...}};
	}

	template <std::size_t... I>
	std::vector<option_info> get_information_vector(std::index_sequence<I...>) const {
		return {(std::get<I>(m_params).info())...};
	}

	std::tuple<Parameters...> m_params;
	std::unordered_map<std::string, option_data> m_adders;
	std::unordered_map<char, std::string> m_short_long_map;
	std::vector<option_data> m_positional_args;
};

template <typename... Params>
inline auto parse(std::vector<std::string> args, Params... params) {
	auto parser = arg_parser{std::move(params)...};
	return parser.parse(std::move(args));
}

template <typename... Params>
inline auto parse(int argc, char** argv, Params... params) {
	return parse(std::vector<std::string>{argv + 1, argv + argc}, std::move(params)...);
}

} // namespace clp

#endif
