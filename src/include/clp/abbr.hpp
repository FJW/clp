#ifndef CLP_ABBR_HPP
#define CLP_ABBR_HPP

#include "core.hpp"
#include "options.hpp"
#include "type_support.hpp"

namespace clp::abbr {
using clp::arg_parser;

using deps = clp::dependencies;
using desc = clp::description;

template <impl::fixed_string Name, typename T>
using req = required_argument<Name, T>;

template <impl::fixed_string Name, typename T>
using opt = optional_argument<Name, T>;

template <impl::fixed_string Name, typename T>
using def = defaulted_argument<Name, T>;

template <impl::fixed_string Name>
using flg = bool_flag<Name>;

template <impl::fixed_string Name>
using ctr = bool_counter<Name>;

template <impl::fixed_string Name, typename T>
using col = collected_argument<Name, T>;

template <impl::fixed_string Name, typename T>
using pos = required_positional_argument<Name, T>;

} // namespace clp::short

#endif // CLP_ABBR_HPP
