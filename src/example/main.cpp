#include <iostream>
#include <string>

#include <clp/clp.hpp>

int main(int argc, char** argv) {
	// Use abbreviated names such as `req` instead of `clp::required_argument`:
	using namespace clp::abbr;
	const auto parser = arg_parser{
	        req<"i,int", int>{desc{"An integer that has to be provided"}},
	        opt<"float", double>{desc{"An optional float without short name."}},
	        def<"s,str", std::string>{"some default value", desc{"a string with a default"}},
	        flg<"h,help">{desc{"a flag that indicates whether the argument was passed"}},
	        ctr<"v,verbose">{desc{"a counter of how often the argument was passed"}},
	        opt<"foo", double>{deps{{"float", "verbose"}},
	                           desc{"passing foo requires float and verbose"}}};
	try {
		const auto parsed = parser.parse(argc, argv);
		// for few arguments tuple-unpacking is easiest:
		const auto [i, f, s, h, v, foo] = parsed;
		std::cout << i << ", " << f.value_or(23) << ", " << s << '\n';
		if (v > 3) {
			std::cout << "Very verbose output\n";
		}
		// Alternatively the values can be accessed like this:
		if (parsed.get<"help">()) {
			parser.print_help(argv[0], std::cout);
		}
	} catch (clp::bad_argument_list& e) {
		std::cerr << "Error: " << e.what() << '\n';
		parser.print_help(argv[0], std::cerr);
		return 1;
	}
}
